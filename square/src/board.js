
Square.Board = function(w,h,tiles,chips) {
	this._setSize(w,h);

	this.chipHistory = {};
	this.tileHistory = {};

	this.pad = 0.05;
	this.borderWidth = 0.05;
	this.margin = 0.2;

	this.calculateSize();

	this.tiles = tiles;
	this.chips = chips;

	this.states = ["rotating", "idle"];

	this.color_indexes = {
		"blue": 0,
		"green": 1,
	};

	this.modes = [
		"coloring",
		"playing",
		"resizing",
	];
	this.mode = "playing";

	this.color_values = {
		tile: [
			//"#74cae5",
			//"#8dc973",

			//"#AED5DF",
			//"#67FD90",
			//"#FDC667",
			//"#FAB4BE",
			//"#F5F107",
			
			"#555", // blank
			"#D5463E", // full bloom (red)
			"#B756B3", // glass orchid (purple)
			"#6571F2", // darley cheese (blue)
			"#4B7651", // great dancers (green)
			"#EEB101", // mustart (yellow)
		],
		"chip_border": "#fefdfb",
		"background": "#252122",
		//"select_border": "#87519f",
		"select_border": "#F00",
	};

	this.numColors = this.color_values.tile.length;

	var that = this;
	this.keydown = function(e) {

		that.cursorTileVal = null;
		if (that.devMode) {
			if (e.shiftKey) {
				that.mode = "resizing";

				if (e.keyCode == 37) { // left
					that.resize(that.w-1, that.h);
				}
				else if (e.keyCode == 38) { // up
					that.resize(that.w, that.h-1);
				}
				else if (e.keyCode == 39) { // right
					that.resize(that.w+1, that.h);
				}
				else if (e.keyCode == 40) { // down
					that.resize(that.w, that.h+1);
				}
				else {
					return;
				}
				e.preventDefault();
			}
			else {
				if (49 <= e.keyCode && e.keyCode < 49 + that.numColors) {
					that.mode = "coloring";
					that.cursorTileVal = e.keyCode - 49;
				}
				else {
					return;
				}
				e.preventDefault();
			}
		}
	};
	this.keyup = function(e) {
		if (that.devMode) {
			if (e.keyCode == 16) {
				that.mode = "playing";
			}
			else if (49 <= e.keyCode && e.keyCode < 49 + that.numColors) {
				that.mode = "playing";
			}
		}
	};
	this.touchHandler = {
		start: function(x,y) {
			if (that.mode == "coloring") {
				that.setTileAtMouse(x,y);
			}
			else if (that.mode == "playing") {
				if (!that.isRotating) {
					that.selectRegionAtMouse(x,y);
					that.isSelecting = true;
				}
			}
		},
		move: function(x,y) {
			if (that.mode == "coloring") {
				that.setTileAtMouse(x,y);
			}
			else if (that.mode == "playing") {
				if (!that.isRotating) {
					that.selectRegionAtMouse(x,y);
					that.isSelecting = true;
				}
			}
		},
		end: function(x,y) {
			if (that.mode == "playing") {
				if (!that.isRotating) {
					//that.selectRegionAtMouse(x,y);
					if (that.selectedIndexes) {
						console.log("starting rotation");
						that.startRotation();
					}
					that.isSelecting = false;
				}
			}
		},
		cancel: function(x,y) {
			that.isSelecting = false;
		},
	};

	this.isRotating = false;
	this.rotateMaxTime = 0.5;
	this.indexOffsets = {};

	// taken from: https://raw.github.com/DmitryBaranovskiy/raphael/master/raphael.js
	this.ease_funcs = {
        linear: function (n) {
            return n;
        },
        "<": function (n) {
            return Math.pow(n, 1.7);
        },
        ">": function (n) {
            return Math.pow(n, .48);
        },
        "<>": function (n) {
            var q = .48 - n / 1.04,
                Q = Math.sqrt(.1734 + q * q),
                x = Q - q,
                X = Math.pow(Math.abs(x), 1 / 3) * (x < 0 ? -1 : 1),
                y = -Q - q,
                Y = Math.pow(Math.abs(y), 1 / 3) * (y < 0 ? -1 : 1),
                t = X + Y + .5;
            return (1 - t) * 3 * t * t + t * t * t;
        },
        backIn: function (n) {
            var s = 1.70158;
            return n * n * ((s + 1) * n - s);
        },
        backOut: function (n) {
            n = n - 1;
            var s = 1.70158;
            return n * n * ((s + 1) * n + s) + 1;
        },
        elastic: function (n) {
            if (n == !!n) {
                return n;
            }
            return Math.pow(2, -10 * n) * Math.sin((n - .075) * (2 * Math.PI) / .3) + 1;
        },
        bounce: function (n) {
            var s = 7.5625,
                p = 2.75,
                l;
            if (n < (1 / p)) {
                l = s * n * n;
            } else {
                if (n < (2 / p)) {
                    n -= (1.5 / p);
                    l = s * n * n + .75;
                } else {
                    if (n < (2.5 / p)) {
                        n -= (2.25 / p);
                        l = s * n * n + .9375;
                    } else {
                        n -= (2.625 / p);
                        l = s * n * n + .984375;
                    }
                }
            }
            return l;
        }
    };

};

Square.Board.prototype = {

	randomizeTiles: function() {
		for (i=0; i<this.len; i++) {
			this.chips[i] = this.tiles[i] = Math.floor(Math.random()*this.numColors);
		}
	},

	// UTILITY FUNCTIONS

	bound: function(val,min,max) {
		return Math.max(min, Math.min(max, val));
	},
	shuffle: function(array) {
		var i,len = array.length;
		var j;
		for (i=0; i<len; i++) {
			j = i + Math.floor(Math.random() * (len-i));
			temp = array[i];
			array[i] = array[j];
			array[j] = temp;
		}
	},

	// CHIP FUNCTIONS

	correctChips: function() {
		var i;
		for (i=0; i<this.len; i++) {
			this.chips[i] = this.tiles[i];
		}
		if (this.devMode) {
			Square.loader.backup();
		}
	},
	shuffleChips: function() {
		var i;
		for (i=0; i<100; i++) {
			var x = Math.floor(Math.random()*(this.w-1));
			var y = Math.floor(Math.random()*(this.h-1));
			this.selectRegionAtXy(x,y);
			this.rotate();
		}
		this.clearSelection();
		if (this.devMode) {
			Square.loader.backup();
		}
	},
	isComplete: function() {
		var i;
		for (i=0; i<this.len; i++) {
			if (this.tiles[i] != this.chips[i]) {
				return false;
			}
		}
		return true;
	},

	// SIZE FUNCTIONS

	_setSize: function(w,h) {
		this.w = w;
		this.h = h;
		this.len = w*h;
	},
	resize: function(w,h) {
		this.clearSelection();

		// Save the current chips and tiles, so later tiles/chips will be populated
		// with previous states.
		var i;
		for (i=0; i<this.len; i++) {
			var xy = this.indexToXy(i);
			var xy = xy.x+" "+xy.y;
			var chip = this.chips[i];
			var tile = this.tiles[i];
			this.chipHistory[xy] = chip;
			this.tileHistory[xy] = tile;
		}

		// set new dimensions (with bounds)
		w = this.bound(w, 2, 10);
		h = this.bound(h, 2, 10);
		this._setSize(w,h);

		// empty the current tiles/chips
		this.chips.length = 0;
		this.tiles.length = 0;

		// create new chips/tiles
		for (i=0; i<this.len; i++) {
			var xy = this.indexToXy(i);
			var xy = xy.x+" "+xy.y;
			var chip = this.chipHistory[xy];
			var tile = this.tileHistory[xy];
			this.chips[i] = (chip != null) ? chip : 0;
			this.tiles[i] = (tile != null) ? tile : 0;
		}

		this.calculateSize();
		this.fitWindow();
		if (this.devMode) {
			Square.loader.backup();
		}
	},

	calculateSize: function() {
		this.pixelWidth = this.w + 2*this.borderWidth + this.pad*(this.w+1) + 2*this.margin;
		this.pixelHeight = this.h + 2*this.borderWidth + this.pad*(this.h+1) + 2*this.margin;
		this.pixelAspect = this.pixelWidth / this.pixelHeight;
	},

	setWindow: function(x,y,w,h) {
		this.window = {
			x: x,
			y: y,
			w: w,
			h: h,
		};
		this.fitWindow();
	},

	fitWindow: function() {
		var x = this.window.x;
		var y = this.window.y;
		var w = this.window.w;
		var h = this.window.h;

		var boardAspect = this.pixelAspect;
		var screenAspect = w/h;
		var w0,h0;
		if (boardAspect > screenAspect) {
			w0 = w;
			h0 = w / boardAspect;
		}
		else {
			h0 = h;
			w0 = h * boardAspect;
		}

		var x0 = x + w/2 - w0/2;
		//var y0 = y + h/2 - h0/2;
		var y0 = y;

		this.x = x0;
		this.y = y0;
		this.scale = w0 / this.pixelWidth; // pixels per unit
	},


	// INPUT FUNCTIONS

	enableInput: function() {
		Square.input.addTouchHandler(this.touchHandler);
		window.addEventListener("keydown", this.keydown, false);
		window.addEventListener("keyup", this.keyup, false);
	},
	disableInput: function() {
		Square.input.removeTouchHandler(this.touchHandler);
		window.removeEventListener("keydown", this.keydown, false);
		window.addEventListener("keyup", this.keyup, false);
	},

	// ROTATION FUNCTIONS

	startRotation: function() {
		this.rotateTime = 0;
		this.isRotating = true;
	},

	rotate: function() {
		var i;
		var oldChips = [];
		for (i=0; i<4; i++) {
			oldChips[i] = this.chips[this.selectedIndexes[i]];
		}
		for (i=0; i<4; i++) {
			var j = (i+1) % 4;
			this.chips[this.selectedIndexes[j]] = oldChips[i];
		}
		if (this.devMode) {
			Square.loader.backup();
		}
	},

	// INDEX <-> X,Y FUNCTIONS

	xyToIndex: function(x,y) {
		return x + y*this.w;
	},
	indexToXy: function(i) {
		return {
			x: i % this.w,
			y: Math.floor(i / this.w),
		};
	},

	// MOUSE FUNCTIONS

	// input: x,y in window coords
	// output: scaled x,y unit coords
	getScaledMouseCoords: function(mx,my) {
		var offset = (this.margin + this.borderWidth) * this.scale;

		var w = this.scale * this.pixelWidth - offset*2;
		var h = this.scale * this.pixelHeight - offset*2;

		var x = mx - this.x - offset;
		var y = my - this.y - offset;

		x = x / w * this.w;
		y = y / h * this.h;

		if (x < 0 || x > this.w || y < 0 || y > this.h) {
			return null;
		}

		return {x:x, y:y};
	},

	setTileAtMouse: function(mx,my) {
		var val = this.cursorTileVal;
		var coords = this.getScaledMouseCoords(mx,my);
		if (coords) {
			var x = Math.floor(coords.x);
			var y = Math.floor(coords.y);
			var i = this.xyToIndex(x,y);
			if (val != null) {
				this.tiles[i] = val;
			}
		}
		if (this.devMode) {
			Square.loader.backup();
		}
	},

	// input: x,y in window coords
	selectRegionAtXy: function(x,y) {
		if (0 <= x && x < this.w-1 &&
			0 <= y && y < this.h-1) {
			this.selectedIndexes = [
				this.xyToIndex(x,y),
				this.xyToIndex(x+1,y),
				this.xyToIndex(x+1,y+1),
				this.xyToIndex(x,y+1),
			];
		}
	},
	selectRegionAtMouse: function(mx,my) {
		var coords = this.getScaledMouseCoords(mx,my);
		if (coords) {
			var x = coords.x;
			var y = coords.y;
			x = Math.floor(this.bound(x-0.5, 0, this.w-2));
			y = Math.floor(this.bound(y-0.5, 0, this.h-2));
			this.selectRegionAtXy(x,y);
		}
		else {
			this.clearSelection();
		}
	},

	clearSelection: function() {
		this.selectedIndexes = null;
	},

	// MAIN FUNCTIONS

	update: function(dt) {
		if (this.isRotating) {
			this.rotateTime += dt;
			if (this.rotateTime < this.rotateMaxTime) {
				// compute piece displacement
				this.indexOffsets = {};
				var c = 1 + this.pad;
				var dx = [c,0,-c,0];
				var dy = [0,c,0,-c];
				var n = this.rotateTime / this.rotateMaxTime;
				var t = this.ease_funcs['bounce'](n);
				var i;
				for (i=0; i<4; i++) {
					this.indexOffsets[this.selectedIndexes[i]] = { x:dx[i] * t, y:dy[i] * t};
				}
			}
			else {
				this.isRotating = false;
				this.indexOffsets = {};

				// rotate pieces
				this.rotate();
			}
		}
	},

	draw: function(ctx) {
		ctx.save();
		ctx.translate(this.x, this.y);
		ctx.scale(this.scale,this.scale);

		var x = 0;
		var y = 0;

		// draw the border
		ctx.lineWidth = this.borderWidth;
		ctx.strokeStyle = "#FFF";
		var diff = this.margin + this.borderWidth/2;
		x += diff;
		y += diff;
		ctx.strokeRect(x,y, this.pixelWidth-diff*2, this.pixelHeight-diff*2);
		var diff = this.borderWidth/2 + this.pad;
		x += diff;
		y += diff;

		var lineWidth = this.pad*3;
		var radius = 0.5 - lineWidth/2 - this.pad/2;

		for (i=0; i<this.len; i++) {
			var xy = this.indexToXy(i);
			var x0 = x + xy.x * (1+this.pad);
			var y0 = y + xy.y * (1+this.pad);

			ctx.fillStyle = this.color_values.tile[this.tiles[i]];
			ctx.fillRect(x0,y0,1,1);
		}

		if (this.mode == "playing") {
			for (i=0; i<this.len; i++) {
				var xy = this.indexToXy(i);
				var x0 = x + xy.x * (1+this.pad);
				var y0 = y + xy.y * (1+this.pad);
				if (!this.chips[i]) {
					continue;
				}

				// draw chip
				var offsets = this.indexOffsets[i];
				if (offsets) {
					x0 += offsets.x;
					y0 += offsets.y;
				}
				ctx.beginPath();
				ctx.arc(x0+0.5, y0+0.5, radius,0,Math.PI*2);
				ctx.fillStyle = this.color_values.tile[this.chips[i]];
				ctx.fill();
				ctx.strokeStyle = this.color_values["chip_border"];
				ctx.lineWidth = lineWidth;
				ctx.stroke();
			}


			if (this.selectedIndexes) {
				xy = this.indexToXy(this.selectedIndexes[0]);
				x0 = x + xy.x * (1+this.pad) - this.pad;
				y0 = y + xy.y * (1+this.pad) - this.pad;
				w0 = h0 = 2 + this.pad*3;
				ctx.lineWidth = this.pad;
				ctx.strokeStyle = "#F00";
				ctx.strokeRect(x0,y0,w0,h0);
				if (this.isSelecting) {
					ctx.fillStyle = "rgba(255,0,0,0.25)";
					ctx.fillRect(x0,y0,w0,h0);
				}
			}

		}

		ctx.restore();
	},
};
