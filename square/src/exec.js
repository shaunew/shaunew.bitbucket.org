Square.setScene = function(scene) {
	Square.scene = scene;
	scene.init();
};

Square.exec = (function(){

	var lastTime;
	var minFps = 20;

	function tick(time) {
		try {

			var dt = (lastTime == undefined) ? 0 : Math.min((time-lastTime)/1000, 1/minFps);
			lastTime = time;

			var scene = Square.scene;
			if (!isPaused) { // this condition could wrap this whole block if we want proper pausing
				scene.update(dt);
			}

			var ctx = Square.screen.ctx;
			scene.draw(ctx);
			requestAnimationFrame(tick);
		}
		catch (e) {
			console.error(e.message + "@" + e.sourceURL);
			console.error(e.stack);
		}
	};

	var isPaused = false;
	function pause() {
		isPaused = true;
	};
	function resume() {
		isPaused = false;
	};

	function start() {
		requestAnimationFrame(tick);
	};

	return {
		start: start,
		pause: pause,
		isPaused: function() { return isPaused; },
		togglePause: function() {
			if (isPaused) {
				resume();
			}
			else {
				pause();
			}
		},
		resume: resume,
	};
})();
