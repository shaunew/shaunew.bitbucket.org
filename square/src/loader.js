
Square.loader = (function(){

	function getState() {
		var board = Square.scene.board;
		return {
			version: 1,
			w: board.w,
			h: board.h,
			tiles: board.tiles,
			chips: board.chips,
		};
	}

	function setState(state) {
		var board = Square.scene.board;
		board._setSize(state.w, state.h);
		board.tiles = state.tiles;
		board.chips = state.chips;
		board.calculateSize();
		board.fitWindow();
		backup();
	}

	function backup() {
		var state = getState();
		var stateStr = JSON.stringify(state,null,'\t');
		if (window.localStorage != undefined) {
			window.localStorage.mooshState = stateStr;
		}
		var btn = document.getElementById("save-button");
		btn.href = "data:application/json;base64," + btoa(stateStr);
		btn.download = "board.json";
	}

	function restore() {
		try {
			if (window.localStorage) {
				var state = JSON.parse(window.localStorage.mooshState);
				if (state) {
					setState(state);
					return true;
				}
			}
		}
		catch (e) {
		}
		return false;
	}

	function openFile(f) {
		var reader = new FileReader();
		reader.onload = function(e) {
			try {
				var state = JSON.parse(e.target.result);
				setState(state);
			}
			catch (e) {
				bootbox.alert("Could not load file '"+f.name+"'");
			}
		};
		reader.readAsText(f);
	}

	// open file dialog
	function handleOpenFile(evt) {
		evt.stopPropagation();
		evt.preventDefault();

		var files = evt.target.files;
		if (files) {
			openFile(files[0]);
		}
		else {
			files = evt.dataTransfer.files;
			if (files) {
				openFile(files[0]);
			}
		}
	}

	return {
		backup: backup,
		restore: restore,
		handleOpenFile: handleOpenFile,
	};
})();
