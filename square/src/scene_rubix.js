
Square.scene_rubix = new function() {
	var that = this;

	this.init = function() {
		this.headerHeight = 100;

		var w = 4;
		var h = 4;
		var a = 1;
		var b = 3;
		var c = 5;
		var chips = [
			0,0,a,a,
			0,0,a,a,
			b,b,c,c,
			b,b,c,c,
		];
		var tiles = [
			0,0,a,a,
			0,0,a,a,
			b,b,c,c,
			b,b,c,c,
		];
		this.board = new Square.Board(w,h,chips,tiles);
		this.rescale();
		Square.screen.onResize = function() { that.rescale(); };
		this.board.enableInput();
	};

	this.cleanup = function() {
		this.board.disableInput();
	};

	this.update = function(dt) {
		this.board.update(dt);
	};

	this.rescale = function() {
		var h0 = this.headerHeight;
		var w = Square.screen.width;
		var h = Square.screen.height-h0;
		this.board.setWindow(0,h0,w,h);
	};

	this.drawTitle = function(ctx) {
		ctx.textBaseline = "middle";
		ctx.textAlign = "center";
		ctx.font = "40px sans-serif";
		ctx.fillStyle = "#FFF";
		var h = this.headerHeight;
		var w = Square.screen.width;
		var mode = this.board.mode;
		var text = "";
		var color = "#555";
		if (mode == "coloring") {
			text = "coloring \u25A0";
			color = this.board.color_values.tile[this.board.cursorTileVal];
		}
		else if (mode == "resizing") {
			text = "resizing " + this.board.h + "x" + this.board.w;
		}
		else if (mode == "playing") {
			if (this.board.isComplete()) {
				//text = "#";
				text = "\u2713";
			}
		}
		ctx.fillStyle = color;
		ctx.fillText(text, w/2,h/2);
	},

	this.draw = function(ctx) {
		ctx.fillStyle = this.board.color_values["background"];
		ctx.fillRect(0,0, Square.screen.width, Square.screen.height);
		this.drawTitle(ctx);
		this.board.draw(ctx);
	};
};
