
Square.screen = new function(){

	var that = this;

	this.setStartSize = function(w,h) {
		this.width = w;
		this.height = h;
		this.aspect = w/h;
	};

	this.setDefaultStartSize = function() {
		this.aspect = 10/16;
		this.width = 320;
		this.height = this.width / this.aspect;
	};

	this.stretchToWindow = function() {
		this.aspect = 10/16;
		this.canvas.height = this.height = window.innerHeight;
		this.canvas.width = this.width = this.height * this.aspect;
		if (this.onResize) {
			this.onResize();
		}
	};

	this.getCanvasPos = function() {
		var p = {x:0,y:0};
		if (navigator.isCocoonJS) {
			return p;
		}
		var obj = that.canvas;
		var addOffset = function(obj) {
			p.x += obj.offsetLeft;
			p.y += obj.offsetTop;
		};
		addOffset(obj);
		while (obj = obj.offsetParent) {
			addOffset(obj);
		}
		return p;
	};

	this.init = function() {
		// Create the canvas element.
		// (CocoonJS provides a more efficient screencanvas if you're using one main canvas).

		if (navigator.isCocoonJS) {
			that.canvas = document.createElement('screencanvas');
			that.setStartSize(window.innerWidth, window.innerHeight);
			that.canvas.width = that.width;
			that.canvas.height = that.height;
			document.body.appendChild(that.canvas);
		}
		else {
			that.canvas = document.getElementById('canvas');
			that.setDefaultStartSize();
			that.canvas.width = that.width;
			that.canvas.height = that.height;
			that.stretchToWindow();
			window.addEventListener("resize", function() {
				that.stretchToWindow();
			},false);
		}
		that.ctx = that.canvas.getContext("2d");
	};

	this.center = function() {
		return;
		var screenW = document.body.clientWidth;
		var screenH = document.body.clientHeight;
		that.canvas.style.position = "relative";
		x = Math.max(0,(screenW/2 - that.width/2));
		y = Math.max(0,(screenH/2 - that.height/2));
		that.canvas.style.left = x+"px";
		that.canvas.style.top = y+"px";
		//body.style.backgroundPosition = (x-132)+"px " + (y-35)+"px";
	};
};
