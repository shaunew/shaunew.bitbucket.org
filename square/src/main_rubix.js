Square = {};

window.addEventListener('load', function() {
	Square.screen.init();
	Square.input.init();
	Square.setScene(Square.scene_rubix);
	Square.exec.start();
});
